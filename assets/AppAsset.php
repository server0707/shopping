<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
//        "frontend/css/bootstrap.css",
        "frontend/css/style.css",
        "frontend/css/font-awesome.css",
        "frontend/css/popuo-box.css",
        "frontend/css/jquery-ui1.css",
    ];
    public $js = [
        "frontend/js/jquery-2.1.4.min.js",
        "frontend/js/jquery.magnific-popup.js",
        "frontend/js/minicart.js",
        "frontend/js/jquery-ui.js",
        "frontend/js/jquery.flexisel.js",
        "frontend/js/SmoothScroll.min.js",
        "frontend/js/move-top.js",
        "frontend/js/easing.js",
//        "frontend/js/bootstrap.js",
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
