<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "category_type".
 *
 * @property int $id
 * @property string|null $category_type_oz
 * @property string|null $category_type_en
 * @property string|null $category_type_ru
 * @property string|null $alias
 * @property string|null $image
 *
 * @property Catalog[] $catalogs
 */
class CategoryType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_type_oz', 'category_type_en', 'category_type_ru', 'alias', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_type_oz' => 'Category Type Oz',
            'category_type_en' => 'Category Type En',
            'category_type_ru' => 'Category Type Ru',
            'alias' => 'Alias',
            'image' => 'Image',
        ];
    }

    /**
     * Gets query for [[Catalogs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogs()
    {
        return $this->hasMany(Catalog::className(), ['category_type_id' => 'id']);
    }
}
