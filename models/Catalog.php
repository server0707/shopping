<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "catalog".
 *
 * @property int $id
 * @property string|null $catalog_name_oz
 * @property string|null $catalog_name_en
 * @property string|null $catalog_name_ru
 * @property string|null $alias
 * @property int $category_type_id
 *
 * @property CategoryType $categoryType
 * @property Product[] $products
 */
class Catalog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'catalog';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_type_id'], 'required'],
            [['category_type_id'], 'integer'],
            [['catalog_name_oz', 'catalog_name_en', 'catalog_name_ru', 'alias'], 'string', 'max' => 255],
            [['category_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => CategoryType::className(), 'targetAttribute' => ['category_type_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'catalog_name_oz' => 'Catalog Name Oz',
            'catalog_name_en' => 'Catalog Name En',
            'catalog_name_ru' => 'Catalog Name Ru',
            'alias' => 'Alias',
            'category_type_id' => 'Category Type ID',
        ];
    }

    /**
     * Gets query for [[CategoryType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryType()
    {
        return $this->hasOne(CategoryType::className(), ['id' => 'category_type_id']);
    }

    /**
     * Gets query for [[Products]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['catalog_id' => 'id']);
    }
}
