<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string|null $alias
 * @property string|null $product_name_oz
 * @property string|null $product_name_en
 * @property string|null $product_name_ru
 * @property string|null $details_oz
 * @property string|null $details_en
 * @property string|null $details_ru
 * @property string|null $price_name_oz
 * @property string|null $price_name_en
 * @property string|null $price_name_ru
 * @property string|null $no_discount_oz
 * @property string|null $no_discount_en
 * @property string|null $no_discount_ru
 * @property string|null $image1
 * @property string|null $image2
 * @property string|null $image3
 * @property string|null $image4
 * @property string|null $status
 * @property string|null $height
 * @property string|null $length
 * @property float|null $evaluation
 * @property string|null $width
 * @property int $catalog_id
 * @property int $state_id
 * @property int $brand_id
 * @property int $goods_id
 * @property int $category_id
 *
 * @property Brand $brand
 * @property Catalog $catalog
 * @property Category $category
 * @property Goods $goods
 * @property State $state
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['details_oz', 'details_en', 'details_ru', 'status'], 'string'],
            [['evaluation'], 'number'],
            [['catalog_id', 'state_id', 'brand_id', 'goods_id', 'category_id'], 'required'],
            [['catalog_id', 'state_id', 'brand_id', 'goods_id', 'category_id'], 'integer'],
            [['alias', 'product_name_oz', 'product_name_en', 'product_name_ru', 'price_name_oz', 'price_name_en', 'price_name_ru', 'no_discount_oz', 'no_discount_en', 'no_discount_ru', 'image1', 'image2', 'image3', 'image4'], 'string', 'max' => 255],
            [['height', 'length', 'width'], 'string', 'max' => 45],
            [['brand_id'], 'exist', 'skipOnError' => true, 'targetClass' => Brand::className(), 'targetAttribute' => ['brand_id' => 'id']],
            [['catalog_id'], 'exist', 'skipOnError' => true, 'targetClass' => Catalog::className(), 'targetAttribute' => ['catalog_id' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['goods_id'], 'exist', 'skipOnError' => true, 'targetClass' => Goods::className(), 'targetAttribute' => ['goods_id' => 'id']],
            [['state_id'], 'exist', 'skipOnError' => true, 'targetClass' => State::className(), 'targetAttribute' => ['state_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alias' => 'Alias',
            'product_name_oz' => 'Product Name Oz',
            'product_name_en' => 'Product Name En',
            'product_name_ru' => 'Product Name Ru',
            'details_oz' => 'Details Oz',
            'details_en' => 'Details En',
            'details_ru' => 'Details Ru',
            'price_name_oz' => 'Price Name Oz',
            'price_name_en' => 'Price Name En',
            'price_name_ru' => 'Price Name Ru',
            'no_discount_oz' => 'No Discount Oz',
            'no_discount_en' => 'No Discount En',
            'no_discount_ru' => 'No Discount Ru',
            'image1' => 'Image1',
            'image2' => 'Image2',
            'image3' => 'Image3',
            'image4' => 'Image4',
            'status' => 'Status',
            'height' => 'Height',
            'length' => 'Length',
            'evaluation' => 'Evaluation',
            'width' => 'Width',
            'catalog_id' => 'Catalog ID',
            'state_id' => 'State ID',
            'brand_id' => 'Brand ID',
            'goods_id' => 'Goods ID',
            'category_id' => 'Category ID',
        ];
    }

    /**
     * Gets query for [[Brand]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brand::className(), ['id' => 'brand_id']);
    }

    /**
     * Gets query for [[Catalog]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCatalog()
    {
        return $this->hasOne(Catalog::className(), ['id' => 'catalog_id']);
    }

    /**
     * Gets query for [[Category]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * Gets query for [[Goods]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGoods()
    {
        return $this->hasOne(Goods::className(), ['id' => 'goods_id']);
    }

    /**
     * Gets query for [[State]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(State::className(), ['id' => 'state_id']);
    }
}
