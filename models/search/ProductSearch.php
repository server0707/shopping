<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Product;

/**
 * ProductSearch represents the model behind the search form of `app\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'catalog_id', 'state_id', 'brand_id', 'goods_id', 'category_id'], 'integer'],
            [['alias', 'product_name_oz', 'product_name_en', 'product_name_ru', 'details_oz', 'details_en', 'details_ru', 'price_name_oz', 'price_name_en', 'price_name_ru', 'no_discount_oz', 'no_discount_en', 'no_discount_ru', 'image1', 'image2', 'image3', 'image4', 'status', 'height', 'length', 'width'], 'safe'],
            [['evaluation'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'evaluation' => $this->evaluation,
            'catalog_id' => $this->catalog_id,
            'state_id' => $this->state_id,
            'brand_id' => $this->brand_id,
            'goods_id' => $this->goods_id,
            'category_id' => $this->category_id,
        ]);

        $query->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'product_name_oz', $this->product_name_oz])
            ->andFilterWhere(['like', 'product_name_en', $this->product_name_en])
            ->andFilterWhere(['like', 'product_name_ru', $this->product_name_ru])
            ->andFilterWhere(['like', 'details_oz', $this->details_oz])
            ->andFilterWhere(['like', 'details_en', $this->details_en])
            ->andFilterWhere(['like', 'details_ru', $this->details_ru])
            ->andFilterWhere(['like', 'price_name_oz', $this->price_name_oz])
            ->andFilterWhere(['like', 'price_name_en', $this->price_name_en])
            ->andFilterWhere(['like', 'price_name_ru', $this->price_name_ru])
            ->andFilterWhere(['like', 'no_discount_oz', $this->no_discount_oz])
            ->andFilterWhere(['like', 'no_discount_en', $this->no_discount_en])
            ->andFilterWhere(['like', 'no_discount_ru', $this->no_discount_ru])
            ->andFilterWhere(['like', 'image1', $this->image1])
            ->andFilterWhere(['like', 'image2', $this->image2])
            ->andFilterWhere(['like', 'image3', $this->image3])
            ->andFilterWhere(['like', 'image4', $this->image4])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'height', $this->height])
            ->andFilterWhere(['like', 'length', $this->length])
            ->andFilterWhere(['like', 'width', $this->width]);

        return $dataProvider;
    }
}
