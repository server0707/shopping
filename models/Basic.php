<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "basic".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $details
 * @property string|null $image
 */
class Basic extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'basic';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'details'], 'string', 'max' => 45],
            [['image'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'details' => 'Details',
            'image' => 'Image',
        ];
    }
}
