<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Product */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'alias',
            'product_name_oz',
            'product_name_en',
            'product_name_ru',
            'details_oz:ntext',
            'details_en:ntext',
            'details_ru:ntext',
            'price_name_oz',
            'price_name_en',
            'price_name_ru',
            'no_discount_oz',
            'no_discount_en',
            'no_discount_ru',
            'image1',
            'image2',
            'image3',
            'image4',
            'status',
            'height',
            'length',
            'evaluation',
            'width',
            'catalog_id',
            'state_id',
            'brand_id',
            'goods_id',
            'category_id',
        ],
    ]) ?>

</div>
