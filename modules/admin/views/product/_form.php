<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'product_name_oz')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'product_name_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'product_name_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'details_oz')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'details_en')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'details_ru')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'price_name_oz')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price_name_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price_name_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_discount_oz')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_discount_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_discount_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image4')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'height')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'length')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'evaluation')->textInput() ?>

    <?= $form->field($model, 'width')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'catalog_id')->textInput() ?>

    <?= $form->field($model, 'state_id')->textInput() ?>

    <?= $form->field($model, 'brand_id')->textInput() ?>

    <?= $form->field($model, 'goods_id')->textInput() ?>

    <?= $form->field($model, 'category_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
