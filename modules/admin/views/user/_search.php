<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\UserSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
<div class="row">
    <div class="col-5"><?= $form->field($model, 'username') ?></div>
    <div class="col-5"><?= $form->field($model, 'name') ?></div>
<!--    <div class="col-2">--><?php // echo $form->field($model, 'isAdmin') ?><!--</div>-->
    <div class="col-2"><?= $form->field($model, 'isAdmin')->dropDownList(['1'=>'Admin','0'=>'User'],['prompt' => '']) ?></div>
</div>
<!--    --><?//= $form->field($model, 'id') ?>

<!--    --><?//= $form->field($model, 'password') ?>

<!--    --><?//= $form->field($model, 'image') ?>

    <div class="form-group text-right">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
