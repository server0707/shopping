<?php

use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CategoryTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Category Types';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-type-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Category Type', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'category_type_oz',
            'category_type_en',
            'category_type_ru',
            'alias',
            //'image',

            ['class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a(FA::icon('eye'), $url, [
                            'title' => 'View',
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a(FA::icon('pencil-alt'), $url, [
                            'title' => 'Update',
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a(FA::icon('trash-alt'), $url, [
                            'title' => 'Delete',
                            'class'=>'text-danger',
                            'data' => [
                                'method' => 'post',
                                'confirm' =>'Ma\'lumotlarni o\'chirmoqchimisiz?',
                            ]
                        ]);
                    },
                ]
            ],
        ],
    ]); ?>


</div>
